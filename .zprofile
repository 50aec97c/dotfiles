mkdir -p /tmp/.cache-$USER
mkdir -p /tmp/.download-$USER

PATH="$HOME/bin:$PATH"
EDITOR=hx
LESSHISTFILE="-"
GIMP2_DIRECTORY="$HOME/.cache/gimp2"

export PATH EDITOR LESSHISTFILE GIMP2_DIRECTORY

NNN_FCOLORS="0B0B04060006060009060B06"
NNN_OPENER="$HOME/.config/nnn/plugins/nuke"
NNN_TMPFILE="$HOME/.cache/.nnn_lastd"
NNN_SEL="$HOME/.cache/.nnn_sel"
export NNN_FCOLORS NNN_OPENER NNN_TMPFILE NNN_SEL
