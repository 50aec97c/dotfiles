call plug#begin()
Plug 'arcticicestudio/nord-vim'
Plug 'itchyny/lightline.vim'
Plug 'andymass/vim-matchup'
Plug 'LunarWatcher/auto-pairs'
Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'lervag/vimtex'
Plug 'tpope/vim-sensible'
call plug#end()

let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors

let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

set hlsearch
set ignorecase
set smartcase

set number
set relativenumber
set cursorline
set cursorlineopt=number

set showcmd

set nobackup
set nowritebackup
set noundofile
set noswapfile
set viminfofile=~/.cache/viminfo

set nostartofline

set hidden

let mapleader = ","

cnoremap <expr> <C-P> wildmenumode() ? "\<C-P>" : "\<Up>"
cnoremap <expr> <C-N> wildmenumode() ? "\<C-N>" : "\<Down>"
nnoremap <C-]> g<C-]>

autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

let g:nord_italic = 1
let g:nord_italic_comments = 1
colorscheme nord

set noshowmode
let g:lightline = {
  \ 'colorscheme': 'nord',
  \ }

"let g:deoplete#enable_at_startup = 1
"
"let g:vimtex_view_method = 'zathura'
"call deoplete#custom#var('omni', 'input_patterns', {
"  \ 'tex': g:vimtex#re#deoplete
"  \})
