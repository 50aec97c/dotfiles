autoload -Uz compinit
compinit -d ~/.cache/zcompdump
zstyle ':completion:*' menu select
zstyle ':completion:*' completer _expand _complete _ignored _match _correct _approximate _prefix

PS1='%F{%(0? 240 %(130? 240 %(127? 196 202)))}─%f '
WORDCHARS=

setopt correctall
setopt globcomplete
setopt histignoredups

bindkey -e
bindkey '^P' history-search-backward
bindkey '^N' history-search-forward
bindkey '^I' expand-or-complete-prefix
bindkey '^[[Z' reverse-menu-complete

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias ls='ls ${=LS_OPTIONS} --quoting-style=shell'
alias grep='grep --color=auto'

test -r ~/.dir_colors && eval $(dircolors ~/.dir_colors)

set_title() {
  echo -en "\e]2;$@\a"
}

precmd() {
  set_title "$PWD"
}

preexec() {
  set_title "$1"
}

n ()
{
  # Block nesting of nnn in subshells
  [ "${NNNLVL:-0}" -eq 0 ] || {
      echo "nnn is already running"
      return
  }

  command nnn "$@"

  [ ! -f "$NNN_TMPFILE" ] || {
      . "$NNN_TMPFILE"
      rm -f -- "$NNN_TMPFILE" > /dev/null
  }
}
